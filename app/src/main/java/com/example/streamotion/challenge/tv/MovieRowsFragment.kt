package com.example.streamotion.challenge.tv

import androidx.leanback.app.RowsSupportFragment
import androidx.leanback.widget.ArrayObjectAdapter
import androidx.leanback.widget.FocusHighlight.ZOOM_FACTOR_MEDIUM
import androidx.leanback.widget.ListRow
import androidx.leanback.widget.ListRowPresenter
import com.example.streamotion.challenge.Movie

class MovieRowsFragment : RowsSupportFragment() {

    fun setItems(movies: List<Movie>) {
        val itemsAdapter = ArrayObjectAdapter(MovieCardPresenter())
        itemsAdapter.setItems(movies, null)
        val firstRow = ListRow(itemsAdapter)
        val rowsAdapter = ArrayObjectAdapter(ListRowPresenter(ZOOM_FACTOR_MEDIUM))
        rowsAdapter.setItems(listOf(firstRow), null)
        adapter = rowsAdapter
    }
}
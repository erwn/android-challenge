package com.example.streamotion.challenge

import com.example.streamotion.challenge.di.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val movieListService: MovieService,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) {

    suspend fun getMovieList(): Result<List<Movie>> =
        withContext(ioDispatcher) {
            return@withContext try {
                Success(movieListService.getMovieList())
            } catch (e: Exception) {
                Error(e)
            }
        }
}
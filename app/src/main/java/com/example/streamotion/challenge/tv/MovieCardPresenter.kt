package com.example.streamotion.challenge.tv

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.leanback.widget.Presenter
import com.example.streamotion.challenge.Movie
import com.example.streamotion.challenge.databinding.CardMovieBinding

class MovieCardPresenter : Presenter() {

    override fun onCreateViewHolder(parent: ViewGroup?): ViewHolder {
        val inflater = LayoutInflater.from(parent?.context)
        val binding = CardMovieBinding.inflate(inflater, parent, false)
        return MovieCardViewHolder(binding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder?, item: Any?) {
        viewHolder as MovieCardViewHolder
        item as Movie
        viewHolder.bind(item)
    }

    override fun onUnbindViewHolder(viewHolder: ViewHolder?) {
    }
}
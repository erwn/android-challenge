package com.example.streamotion.challenge.mobile

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.streamotion.challenge.*
import com.example.streamotion.challenge.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val progressBar get() = binding.progressBar

    private val resultsText get() = binding.resultsText

    private val viewModel: MovieListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel.movies.observe(this, ::handleResult)
    }

    private fun handleResult(result: Result<List<Movie>>) {
        when (result) {
            is Success -> {
                progressBar.isVisible = false
                displayData(result.data)
            }
            is Loading -> {
                resultsText.isVisible = false
                progressBar.isVisible = true
            }
            is Error -> {
                resultsText.isVisible = false
                progressBar.isVisible = false
                Toast.makeText(this, result.exception.message, Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun displayData(movies: List<Movie>) {
        resultsText.text = movies.joinToString("\n\n") { it.toString() }
    }
}
package com.example.streamotion.challenge.tv

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentActivity
import com.example.streamotion.challenge.*
import com.example.streamotion.challenge.databinding.ActivityTvMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : FragmentActivity() {

    private lateinit var binding: ActivityTvMainBinding

    private val progressBar get() = binding.progressBar

    private val movieRowsFragment
        get() = supportFragmentManager.findFragmentById(R.id.rowsContainer) as MovieRowsFragment

    private val viewModel: MovieListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTvMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel.movies.observe(this, ::handleResult)
    }

    private fun handleResult(result: Result<List<Movie>>) {
        when (result) {
            is Success -> {
                progressBar.isVisible = false
                movieRowsFragment.setItems(result.data)
            }
            is Loading -> {
                progressBar.isVisible = true
            }
            is Error -> {
                progressBar.isVisible = false
                Toast.makeText(this, result.exception.message, Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }
}
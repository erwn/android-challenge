package com.example.streamotion.challenge.tv

import androidx.leanback.widget.Presenter
import com.bumptech.glide.Glide
import com.example.streamotion.challenge.Movie
import com.example.streamotion.challenge.databinding.CardMovieBinding

class MovieCardViewHolder(
    private val binding: CardMovieBinding
) : Presenter.ViewHolder(binding.root) {

    fun bind(movie: Movie) {
        with(binding) {
            title.text = movie.title

            Glide.with(root.context)
                .load(movie.imageUrl)
                .into(poster)
        }
    }
}
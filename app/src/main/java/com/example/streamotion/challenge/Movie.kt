package com.example.streamotion.challenge

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Movie(
    @Json(name = "id")
    val id: String,
    @Json(name = "title")
    val title: String,
    @Json(name = "image")
    val imageUrl: String
) {
    override fun toString(): String =
        "$title ($id)\nPoster URL: $imageUrl"
}
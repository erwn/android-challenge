package com.example.streamotion.challenge

import retrofit2.http.GET

interface MovieService {

    @GET("/staging/common/android-challenge/movies.json")
    suspend fun getMovieList(): List<Movie>
}
# Streamotion Android Challenge

Thank you for taking the time to do this challenge.

The goal of this exercise is to be able to implement different requirements to add functionality to
a very simple mobile and TV app.

The majority of the boilerplate code is already in place and the your task is to write the remaining
pieces.

If you feel there's an ambiguity in any of the requirements, please use your best judgment. We're
not really looking for exact solutions here but instead want to gauge your approach on tackling
feature-related work.

Good luck!

## Overview

You would need the latest stable [Android Studio](https://developer.android.com/studio) to run the
project. To be able to run the mobile or TV project you would need to switch the run configuration.

## Time Estimate

This exercise could take approximately 3-4 hours of focused time to complete. However don't feel
pressured to complete all of the requirements if you find yourself spending way more than that.

You can cut down on the number of requirements you chose to submit. We'd much prefer quality over
quantity :).

Ideally whatever requirement(s) you complete should showcase your best work.

## Mobile App Requirements

Given an [API
endpoint](https://resources.streamotion.com.au/staging/common/android-challenge/movies.json) that
returns list of movies, we want to you to implement the following:

1. Display the list of movies in a `RecyclerView`. The movie poster and title should both be
   visible for each item.
2. In portrait mode display 2 movie items per row, and on landscape display 3 items.
3. Implement a way for the user to mark certain movie titles as "favourites". You can use the
   `VectorDrawable`s: `ic_star_not_selected` and `ic_star_selected` for this. Ideally the items
   marked as favourites should persist even after an app teardown. Use the device storage for saving
   this information.
4. Add a mechanism to sort the list of movies. Sort options should include: a) Favourites-first,
   b) By item id. Feel free to come up with your own layout/design for this.

## TV App Requirements

The TV app should start with a screen with a row of movie titles. Given this initial screen
(which shares the same data layer as mobile), implement the following requirements:

1. As each movie is focused, display that movies title in the background. Make sure the title is
   centered in the background with a legible font size.
2. Add an "exit gating" prompt. If the user presses the DPAD back button, then this confirmation
   prompt should appear. It should look something like this:

   (insert GIF here)

## Grading

- For each requirement, it would be ideal if you can implement them in separate `git` commits.
  For each commit, we request you write a concise and clear commit message.
- We're looking for clean, simple, readable and architecturally sound code.
- Whilst we're after testable code, we're not really too fussed about having unit tests for this
  challenge. However if you've got time to spare and are up to it, then by all means include tests.
- Be prepared to explain your approach when implementing the different requirements.

## Submission

- Please send us the `.zip` archive of your local `git` repository with all your changes. Refer to
  the `git archive` command for how to do this.